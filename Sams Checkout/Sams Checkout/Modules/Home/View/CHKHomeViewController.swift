//
//  ViewController.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/9/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//

import UIKit

class CHKHomeViewController: UIViewController {

    // Product Table View
    @IBOutlet weak var productsTableView: UITableView!
    // Presenter
    var presenter: HomeViewToPresenterProtocol?
    
    // Products
    var tableProducts = [CHKProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Refresh Product List
        presenter?.refreshProductList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Custom table View Cell Info
        productsTableView.estimatedRowHeight = 144
        productsTableView.rowHeight = UITableViewAutomaticDimension
    }
}

extension CHKHomeViewController: HomePresenterToViewProtocol {
    
    // Fetch Sucess
    func productsDidLoad(products: [CHKProduct]) {
        self.tableProducts = products
        DispatchQueue.main.async {
            self.productsTableView.reloadData()
        }
    }
    
    // Fetch Failed
    func productsFailedToLoadWith(error: CHKError) {
    }
}

extension CHKHomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.tableProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CHKProductCell = productsTableView.dequeueReusableCell(withIdentifier: CHKConstants.productCellIdentifier) as! CHKProductCell
        cell.productName.text = tableProducts[indexPath.row].productName ?? ""
        cell.productDescription.text = tableProducts[indexPath.row].shortDescription ?? ""
        cell.productImage.loadImageFrom(url: tableProducts[indexPath.row].productImage ?? "")
        cell.ratingView.setRatingTo(value: Int(tableProducts[indexPath.row].reviewRating ?? 0.0))
        cell.priceLabel.text = tableProducts[indexPath.row].price ?? ""
        cell.inStock(value: tableProducts[indexPath.row].inStock ?? false)
        return cell
    }
    
    // Will Display Cell
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Check if user is about to reach bottom of screen
        if Double(indexPath.row) >= CHKConstants.productRefreshRate * Double(tableProducts.count){
            // Refesh List
            presenter?.refreshProductList()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentSelectedProduct = tableProducts[indexPath.row]
        // Show Details Page of selected product
        presenter?.navigateToDetailsFor(product: currentSelectedProduct, from: self)
    
    }
}

