//
//  CHKHomeViewInteractor.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/9/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//

import Foundation

class CHKHomeViewInteractor: HomePresenterToInteractorProtocol {
    // Presenter
    var presenter: HomeInteractorToPresenterProtocol?

    // Product Service
    var productService = CHKProductsService()
    
    // Products Cache
    var productsCache  = CHKCache.sharedCache
    
    // Fetch Products
    func fetchProducts() {
        // Check if all products are loaded
        if !CHKCache.sharedCache.allProductsLoaded() {
        print("Fetch Products in page \(self.productsCache.currentPage + 1)")
        // Make a Network call to get all details of products
        productService.getProductsFrom(page: self.productsCache.currentPage + 1) { (response, error) in
            // Get Current Page Value
            self.productsCache.currentPage = response!.pageNumber ?? 0
            // Append Products
            self.productsCache.products!.append(contentsOf: response!.products!)
            // Total Products
            self.productsCache.totalProducts = response!.totalProducts
            
            // Call Presentor with success
            self.presenter?.productsFetched(products: self.productsCache.products, error: error)
            }
        }
    }
}
