//
//  CHKProductCell.swift
//  WalmartCheckout
//
//  Created by Karthik S on 3/11/18.
//  Copyright © 2018 Karthik S. All rights reserved.
//

import UIKit

class CHKProductCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var ratingView: CHKRatingView!
    @IBOutlet weak var inStockView: UIView!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
    // In Stock UI
    func inStock(value:Bool) {
        if value {
            self.inStockView.isHidden = false
            return
        }
        self.inStockView.isHidden = true
    }
}
