//
//  CHKHomeViewRouter.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/10/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//

import Foundation
import UIKit

class CHKHomeViewRouter: HomePresenterToRouterProtocol {
    
    // Class Function to create home view controller
    class func createHomeViewController() -> UIViewController {

        // Navigation Controller
        let navController = CHKUtility.navController
        
        if let view = navController.childViewControllers.first as? CHKHomeViewController {
            // Presenter
            let presenter: HomeViewToPresenterProtocol & HomeInteractorToPresenterProtocol = CHKHomeViewPresenter()
            // Interactor
            let interactor: HomePresenterToInteractorProtocol = CHKHomeViewInteractor()
            // Router
            let router: HomePresenterToRouterProtocol = CHKHomeViewRouter()
            
            view.presenter = presenter
            presenter.view = view
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
            
            return navController
        }
        return UIViewController()
    }
}

extension CHKHomeViewRouter {
    
    // Navigate to Details View Controller
    func navigateToDetailViewFrom(view: UIViewController,With product: CHKProduct) {
        let detailsViewController = CHKDetailsViewRoute.createDetailsModuleWith(product: product)
        CHKUtility.navController.pushViewController(detailsViewController, animated: true)
    }
}
