//
//  CHKHomeViewPresenter.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/9/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//

import Foundation
import UIKit

class CHKHomeViewPresenter: HomeViewToPresenterProtocol {
    
    // Set initial properties
    var view: HomePresenterToViewProtocol?
    var interactor: HomePresenterToInteractorProtocol?
    var router: HomePresenterToRouterProtocol?
    
    // Refresh Product List
    func refreshProductList() {
        interactor?.fetchProducts()
    }
    
    // Navigates to details page
    func navigateToDetailsFor(product: CHKProduct, from view: UIViewController) {
        router?.navigateToDetailViewFrom(view: view, With: product)
    }
}

extension CHKHomeViewPresenter: HomeInteractorToPresenterProtocol {
    // Check for Fetch Request
    func productsFetched(products: [CHKProduct]?, error: CHKError?) {
        
        // Check for error
        guard let error = error else {
            // On Success
            self.view?.productsDidLoad(products: products!)
            return
        }
        // On Fetch Failure
        self.view?.productsFailedToLoadWith(error: error)
    }
}
