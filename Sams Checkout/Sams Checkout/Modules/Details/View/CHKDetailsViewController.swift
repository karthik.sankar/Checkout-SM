//
//  CHKDetailsViewController.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/11/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//

import UIKit

class CHKDetailsViewController: UIViewController {
    
    // Set UI Outlets
    @IBOutlet weak var inStock: UILabel!
    @IBOutlet weak var ratingView: CHKRatingView!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    // Current Product displayed on screen.
    var currentProduct: CHKProduct?
    
    // Presenter
    var presenter: DetailViewToPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Adding Gestures to View to enable product switch
        addGesturesToView()
        
        // Load Current Product
        loadProduct()
        
        // Show Alert View for Swipe
        showInfo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

}

// MARK: - Utility
extension CHKDetailsViewController {
    // Add Gestures to View
    func addGesturesToView() {
        // Adding Right Swipe to View
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(didSwipeHappen))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        // Adding Left Swipe to View
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(didSwipeHappen))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        
        // Gestures
        self.view.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    // Load Current Product
    func loadProduct() {
        // Set Name as Navigation Title
        self.title = currentProduct!.productName ?? ""
        // Set Product Image
        productImageView.loadImageFrom(url: currentProduct!.productImage ?? "")
        // Set Price
        productPrice.text = currentProduct!.price ?? ""
        // Set Rating
        ratingView.setRatingTo(value: Int(currentProduct!.reviewRating ?? 0.0))
        // Product Description
        productDescription.text = currentProduct!.longDescription ?? ""
        // In Stock
        inStock.isHidden = !(currentProduct!.inStock ?? false)
    }
    
    // Show Alert
    func showInfo() {
        let alertMessage = UIAlertController(title: "Info", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        action.setValue(UIImage(named: "Alert")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
        alertMessage .addAction(action)
        self.present(alertMessage, animated: true, completion: nil)
    }
}

// MARK: Swipe Actions
extension CHKDetailsViewController {
    // Swipe Handling
    @objc func didSwipeHappen( gestureReg: UIGestureRecognizer) {
        // Check for Swipe Gesture
        if let swipeGesture = gestureReg as? UISwipeGestureRecognizer
        {
            switch swipeGesture.direction
            {
            case .right:
                presenter?.loadPreviousProductFrom(currentProduct: currentProduct!)
            case .left:
                presenter?.loadNextProductFrom(currentProduct: currentProduct!)
            default:
                break
            }
            
            // Load Current Product
            loadProduct()
        }
    }
}

// Presenter Prortodocl
extension CHKDetailsViewController: DetailPresenterToViewProtocol {
    
    // Shows the new product from Presenter
    func loadNewProduct(product: CHKProduct) {
        self.currentProduct = product
        loadProduct()
    }
}
