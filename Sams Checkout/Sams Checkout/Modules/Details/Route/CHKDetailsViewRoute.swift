//
//  CHKDetailsViewRoute.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/11/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//

import Foundation
import UIKit

class CHKDetailsViewRoute: DetailPresenterToRouterProtocol {
    
    // Create UIView Controller
    class func createDetailsModuleWith(product: CHKProduct) -> UIViewController {
        
        // View Controller
        guard let viewController = CHKUtility.mainStoryboard.instantiateViewController(withIdentifier: CHKConstants.detailsViewController) as? CHKDetailsViewController else {
            // No View Controller Present
            print("Cannot Initiate View Controller : Check View Controller Identifier")
            return UIViewController()
        }
        // Presenter
        let presenter: DetailViewToPresenterProtocol & DetailInteractorToPresenterProtocol = CHKDetailsViewPresenter()
        // Interactor
        let interactor: DetailPresenterToInteractorProtocol = CHKDetailsViewInteractor()
        // Router
        let router: DetailPresenterToRouterProtocol = CHKDetailsViewRoute()
        
        // Set all View Parameters
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        viewController.currentProduct = product
        
        return viewController
    }
}
