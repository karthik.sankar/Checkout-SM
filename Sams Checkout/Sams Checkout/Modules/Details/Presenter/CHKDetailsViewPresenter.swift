//
//  CHKDetailsViewPresenter.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/11/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//

import Foundation

class CHKDetailsViewPresenter: DetailViewToPresenterProtocol {
    // Set Initial Variables
    var view: DetailPresenterToViewProtocol?
    var router: DetailPresenterToRouterProtocol?
    var interactor: DetailPresenterToInteractorProtocol?
    
    
    /// Loads Previous Product, uses current product as reference
    ///
    /// - Parameter currentProduct: Current Product
    func loadPreviousProductFrom(currentProduct: CHKProduct) {
        // Check if user needs more products when they reach 80%
        let indexOfCurrentProduct = CHKCache.sharedCache.products?.index(of: currentProduct)
        if indexOfCurrentProduct! != 0 {
            let prevProduct = (CHKCache.sharedCache.products?[indexOfCurrentProduct!-1])!
            // Load New Product
            view?.loadNewProduct(product: prevProduct)
        }
    }
    
    
    /// Loads Next Set of Products, uses current product as reference
    ///
    /// - Parameter currentProduct: Current Product
    func loadNextProductFrom(currentProduct: CHKProduct) {
        // Check if user needs more products when they reach 80%
        let indexOfCurrentProduct = CHKCache.sharedCache.products?.index(of: currentProduct)
        if indexOfCurrentProduct! < (CHKCache.sharedCache.products?.count)!-1 {
            // Refresh Index
            let indexOfRefresh = Double((CHKCache.sharedCache.products?.count)!) * CHKConstants.productRefreshRate
            // Check if New Products needs to be fetched
            if Double(indexOfCurrentProduct!) >= indexOfRefresh {
                // Get New List
                interactor?.loadNextProducts()
            }
            else {
                let nextProduct = (CHKCache.sharedCache.products?[indexOfCurrentProduct!+1])!
                // Show in View
                view?.loadNewProduct(product: nextProduct)
            }
        }
    }
}

// MARK: - Call from Iterator
extension CHKDetailsViewPresenter: DetailInteractorToPresenterProtocol{
    
    
    /// Tells view to load new product from Iterator
    ///
    /// - Parameter newProduct: new Product to be shown in screen.
    func nextProductsFetched(newProduct: CHKProduct) {
        // Show in View
        view?.loadNewProduct(product: newProduct)
    }
}
