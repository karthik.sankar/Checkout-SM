//
//  CHKDetailsViewInteractor.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/11/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//

import Foundation

class CHKDetailsViewInteractor: DetailPresenterToInteractorProtocol {
    
    // Presenter
   var presenter: DetailInteractorToPresenterProtocol?

    // Product Service
    var productService = CHKProductsService()
    
    // Products Cache
    var productsCache  = CHKCache.sharedCache
    
    
    /// Loads Next set of Products using index of current product.
    func loadNextProducts() {
        productService.getProductsFrom(page: ((productsCache.products?.count)!/CHKConstants.defaultPageSize) + 1) { (response, error) in
            // Append Products
            self.productsCache.products!.append(contentsOf: response!.products!)
            // Total Products
            self.productsCache.totalProducts = response!.totalProducts
        }
    }
}
