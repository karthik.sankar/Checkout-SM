//
//  CHKCache.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/9/18.
//  Copyright © 2018 Karthik S. All rights reserved.
//

import Foundation

class CHKCache {
    // List of products from server
    var products : [CHKProduct]?
    // Total Products
    var totalProducts : Int?
    // Curren Page Number
    var currentPage = 0
    
    // Singleton Shared Cache
    static let sharedCache = CHKCache()
    
    // Initialization
    private init(){
        // Initialization
        self.totalProducts = 0
        self.products = [CHKProduct]()
    }
    
    // Check if all products are loaded in cache
    func allProductsLoaded() -> Bool {
        if self.currentPage != 0 && (self.products!.count >= self.totalProducts!) {
            return true
        }
        return false
    }
}
