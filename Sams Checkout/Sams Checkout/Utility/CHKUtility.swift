//
//  CHKUtility.swift
//  WalmartCheckout
//
//  Created by Karthik S on 3/9/18.
//  Copyright © 2018 Karthik S. All rights reserved.
//

import UIKit

// Common Error Struct for Network Requests
struct CHKError  {
    let code : Int?             // Error Code
    let isSuccess : Bool?        // Sucess Call
    let description : String?    //  Description
    let title : String?          // Error Title
}


// UIImage View Extension
extension UIImageView {
    
    // Load Image from url asynchronously
    public func loadImageFrom(url: String) {
        
        // Create a URL Session Task to download image
        URLSession.shared.dataTask(with: NSURL(string: url)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                return
            }
            // Asyn Load to main queue
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
        }).resume()
    }
}

// Utitily Function
class CHKUtility {
    // Main Storyboard
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    // Navigation Controller
    static var navController = mainStoryboard.instantiateViewController(withIdentifier: CHKConstants.navIdentifier) as! UINavigationController
}
