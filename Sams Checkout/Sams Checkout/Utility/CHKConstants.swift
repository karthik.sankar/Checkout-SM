//
//  CHKConstants.swift
//  WalmartCheckout
//
//  Created by Karthik S on 3/9/18.
//  Copyright © 2018 Karthik S. All rights reserved.
//
import Foundation
// Constants used in SamsClub Checkout App
struct CHKConstants {
    // Walmart URL
    static let requestURL = "https://walmartlabs-test.appspot.com/_ah/api/walmart/v1"
    // Walmart API Key
    static let requestAPIKey = "f6c443b0-25d4-4620-9fa4-1759353f9ed8"
    // Walmart Product Parameter
    static let requestProducts = "walmartproducts"
    // Default Page Size Count
    static let defaultPageSize = 20
    
    // Product Cell Identifier
    static let productCellIdentifier = "productCell"
    
    // Segue for Product Detail
    static let productDetailSegue = "showProductDetailView"
    
    // Product Refesh Rate
    static let productRefreshRate = 0.8 // In Percent its 80%, When 80% products are shown refesh will happen to load more items
    
    // Router Constants
    static let homeViewController = "HomeViewController"
    
    static let detailsViewController = "productDetailView"
    
    // Navigation Controller
    static let navIdentifier = "baseNavigationController"
}
