//
//  CHKDetailsProtocol.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/11/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//
/*!
 * CHKHomeProtocols contains protocols used by application in order to transfer data from one component to order. It belongs to Home View Controller
 * - DetailViewToPresenterProtocol - Calls from View to Presenter. Presenter conforms to this protocol.
 * - DetailPresenterToViewProtocol - Calls from Presenter to View. View conforms to this protocol.
 * - DeatilInteractorToPresenterProtocol - Calls from Interactor to Presenter. Presenter conforms to this protocol.
 * - DetailPresenterToInteractorProtocol - Calls from Presenter to Interactor. Presenter conforms to this protocol.
 * - DetailPresenterToRouterProtocol - Calls from Presenter to Router. Router conforms to this protocol.
 */
import Foundation
import UIKit

// MARK: View => Presenter
protocol DetailViewToPresenterProtocol: AnyObject {
    // View
    var view: DetailPresenterToViewProtocol? {get set}
    // Interactor
    var interactor: DetailPresenterToInteractorProtocol? {get set}
    // Router
    var router: DetailPresenterToRouterProtocol? {get set}
    
    // Load Previous Product
    func loadPreviousProductFrom(currentProduct: CHKProduct)
    
    // Load Next Product
    func loadNextProductFrom(currentProduct: CHKProduct)
}

// MARK: Presenter => View
protocol DetailPresenterToViewProtocol: AnyObject {
    
    // Load New Product
    func loadNewProduct(product: CHKProduct)
}

// MARK: Interactor => Presenter
protocol DetailInteractorToPresenterProtocol: AnyObject {
    
    // Show Next Product
    func nextProductsFetched(newProduct: CHKProduct)
}

// MARK: Presenter => Interactor
protocol DetailPresenterToInteractorProtocol: AnyObject {
    // Presenter
    var presenter: DetailInteractorToPresenterProtocol? {get set}
    
    // Loads next set of products
    func loadNextProducts()
}

// MARK: Presenter => Router
protocol DetailPresenterToRouterProtocol: AnyObject {
    // Not Used as there is no navigation beyond detail view
}
