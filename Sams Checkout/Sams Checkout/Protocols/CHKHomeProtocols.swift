//
//  CHKHomeProtocols.swift
//  Sams Checkout
//
//  Created by Karthik S on 3/9/18.
//  Copyright © 2018 Karthik Sankar. All rights reserved.
//
/*!
 * CHKHomeProtocols contains protocols used by application in order to transfer data from one component to order. It belongs to Home View Controller
 * - HomeViewToPresenterProtocol - Calls from View to Presenter. Presenter conforms to this protocol.
 * - HomePresenterToViewProtocol - Calls from Presenter to View. View conforms to this protocol.
 * - HomeInteractorToPresenterProtocol - Calls from Interactor to Presenter. Presenter conforms to this protocol.
 * - HomePresenterToInteractorProtocol - Calls from Presenter to Interactor. Presenter conforms to this protocol.
 * - HomePresenterToRouterProtocol - Calls from Presenter to Router. Router conforms to this protocol.
 */
import Foundation
import UIKit

// MARK: View => Presenter
protocol HomeViewToPresenterProtocol: AnyObject {
    // View
    var view: HomePresenterToViewProtocol? {get set}
    // Interactor
    var interactor: HomePresenterToInteractorProtocol? {get set}
    // Router
    var router: HomePresenterToRouterProtocol? {get set}
    // Refresh Product List
    func refreshProductList()
    // Navigate to Details View
    func navigateToDetailsFor(product: CHKProduct, from view: UIViewController)
}

// MARK: Presenter => View
protocol HomePresenterToViewProtocol: AnyObject {
    // Products CHKProduct
    func productsDidLoad(products: [CHKProduct])
    // Products fetched failed
    func productsFailedToLoadWith(error: CHKError)
}

// MARK: Interactor => Presenter
protocol HomeInteractorToPresenterProtocol: AnyObject {
    // Products Fetched
    func productsFetched(products: [CHKProduct]?,
                         error: CHKError?)
    
}

// MARK: Presenter => Interactor
protocol HomePresenterToInteractorProtocol: AnyObject {
    // Presenter
    var presenter: HomeInteractorToPresenterProtocol? {get set}
    // Fetch Products Call
    func fetchProducts()
}

// MARK: Presenter => Router
protocol HomePresenterToRouterProtocol: AnyObject {
    // Create Home View Controller
    static func createHomeViewController() -> UIViewController
    
    // Move to Detail View
    func navigateToDetailViewFrom(view: UIViewController,With product: CHKProduct)
}
